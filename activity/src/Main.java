import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        // Declaration
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;

        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name:");
        firstName = myObj.nextLine();

        System.out.println("Last Name:");
        lastName = myObj.nextLine();

        System.out.println("First Subject Grade:");
        firstSubject = myObj.nextDouble();

        System.out.println("Second Subject Grade:");
        secondSubject = myObj.nextDouble();

        System.out.println("Third Subject Grade:");
        thirdSubject = myObj.nextDouble();

        double average = (firstSubject + secondSubject + thirdSubject)/3;

        System.out.println("Good day, " + firstName + " " + lastName);
        System.out.println("Your grade average is: " + average);
    }
}